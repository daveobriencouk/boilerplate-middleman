###
# Compass
###

# Change Compass configuration
# compass_config do |config|
#   config.output_style = :compact
# end

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", :layout => false
#
# With alternative layout
# page "/path/to/file.html", :layout => :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (http://middlemanapp.com/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", :locals => {
#  :which_fake_page => "Rendering a fake page with a local variable" }

###
# Helpers
###

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
activate :livereload

# Methods defined in the helpers block are available in templates
helpers do
  def nav_active(path)
    current_page.path == path ? {:class => "active"} : {}
  end
end

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

ignore "/partials/*"


# Build-specific configuration
configure :build do
  # Minify CSS on build
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Create favicons
  activate :favicon_maker

  # Enable cache buster
  # activate :asset_hash

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
end


# Middleman Deploy: https://github.com/middleman-contrib/middleman-deploy
activate :deploy do |deploy|
  deploy.method = :sftp
  deploy.host   = "ego-server.com"
  deploy.path   = ""
  # deploy.path   = "/var/www/vhosts/ego-server.com/subdomains/xxx"

  # Optional Settings
  deploy.user     = "ego-server"
  deploy.password = ""

  deploy.build_before = true
end

after_configuration do
  @bower_config = JSON.parse(IO.read("#{root}/.bowerrc"))
  @bower_assets_path = File.join "#{root}", @bower_config["directory"]
  sprockets.append_path @bower_assets_path
  sprockets.append_path 'javascripts/vendor'
end


# Middleman Autoprefixer: https://github.com/middleman/middleman-autoprefixer
activate :autoprefixer do |config|
  # config.browsers = ['last 2 versions', 'Explorer >= 9']
  # config.cascade  = false
  # config.inline   = true
  # config.ignore   = ['hacks.css']
end


# Middleman ImageOptim: https://github.com/plasticine/middleman-imageoptim
# activate :imageoptim do |options|
#   # Use a build manifest to prevent re-compressing images between builds
#   options.manifest = true

#   # Silence problematic image_optim workers
#   options.skip_missing_workers = true

#   # Cause image_optim to be in shouty-mode
#   options.verbose = false

#   # Setting these to true or nil will let options determine them (recommended)
#   options.nice = true
#   options.threads = true

#   # Image extensions to attempt to compress
#   options.image_extensions = %w(.png .jpg .gif .svg)

#   # Compressor worker options, individual optimisers can be disabled by passing
#   # false instead of a hash
#   options.advpng    = { :level => 4 }
#   options.gifsicle  = { :interlace => false }
#   options.jpegoptim = { :strip => ['all'], :max_quality => 100 }
#   options.jpegtran  = { :copy_chunks => false, :progressive => true, :jpegrescan => true }
#   options.optipng   = { :level => 6, :interlace => false }
#   options.pngcrush  = { :chunks => ['alla'], :fix => false, :brute => false }
#   options.pngout    = false
#   options.svgo      = false
# end


# Middleman Build Reporter: https://github.com/mdb/middleman-build-reporter
activate :build_reporter do |build|
  # optional; the path to your project repository root
  # this must be absolute or relative from your build directory
  # defaults to app root
  # build.repo_root = ''

  # optional; the version of your app
  # defaults to ''
  build.version = '1.2.3'

  # optional; the build reporter file name
  # defaults to 'build'
  build.reporter_file = 'build'

  # optional; an array of desired build reporter file formats
  # supported formats: yaml, json
  # defaults to ['yaml']
  build.reporter_file_formats = ['yaml', 'json']
end
